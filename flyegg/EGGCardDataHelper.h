//
//  EGGLocalDataHelper.h
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 1..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Card;
@class Tag;

@interface EGGCardDataHelper : NSObject

- (NSArray *) allChildren;
- (NSArray *) allCategory;
- (NSArray *) allTags;
- (NSArray *) allCards;

- (NSArray *) findCardsByChildren:(NSString *)childrenName isRandom:(BOOL)isRandom;
- (NSArray *) findCardsByCategory:(NSString *)categoryName isRandom:(BOOL)isRandom;
- (NSArray *) findCardsByTag:(NSString *)tagName isRandom:(BOOL)isRandom;
- (NSArray *) findCardsByWord:(NSString *)word;

- (Card *) addNewCard:(NSString *)word
        withImagePath:(NSString *)imagePath
        withThumbPath:(NSString *)thumbPath;

- (Card *) updateCard:(Card *)targetCard
          withNewWord:(NSString *)newWord
        withImagePath:(NSString *)newImagePath
        withThumbPath:(NSString *)newThumbPath;

- (void) deleteCard:(Card *)targetCard;

- (Card *) registerTag:(Tag *)tag withCard:(Card *)targetCard;
- (Card *) removeTag:(Tag *)tag withCard:(Card *)targetCard;

- (Tag *) addNewTag:(NSString *)word
        withTagType:(NSString *)tagType;

- (Tag *) updateTag:(Tag *)targetTag
        withNewWord:(NSString *)word
        withTagType:(NSString *)tagType;

- (void) deleteTag:(Tag *)targetTag;

@end

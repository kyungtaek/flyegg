//
//  EGGConfigCardCollectionViewController.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 2..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGConfigCardCollectionViewController.h"
#import "EGGThumbCardVIewCell.h"
#import "EGGCategoryView.h"
#import "Card.h"
#import "Tag.h"

@interface EGGConfigCardCollectionViewController ()

@end

@implementation EGGConfigCardCollectionViewController

- (id)init
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(220, 220)];
    [flowLayout setMinimumInteritemSpacing:25.f];
    [flowLayout setMinimumLineSpacing:25.f];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(20.f, 20.f, 20.f, 20.f);
    flowLayout.headerReferenceSize = CGSizeMake(200, 80);
    
    self = [super initWithCollectionViewLayout:flowLayout];
    if (self) {
        self.categoryList = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.collectionView registerClass:[EGGThumbCardVIewCell class]
            forCellWithReuseIdentifier:@"EGGThumbCardVIewCell"];
    
    [self.collectionView registerClass:[EGGCategoryView class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:@"EGGCategoryView"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDatasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    Tag *targetCategory = [self.categoryList objectAtIndex:section];
    return [targetCategory.cards count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.categoryList count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    EGGThumbCardVIewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EGGThumbCardVIewCell" forIndexPath:indexPath];
    
    Tag *targetCategory = [self.categoryList objectAtIndex:indexPath.section];
    Card *targetCard = [targetCategory.cards objectAtIndex:indexPath.row];
    
    [cell setWord:targetCard.word];
    [cell setThumbPath:targetCard.thumbpath];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    
    EGGCategoryView *view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                               withReuseIdentifier:@"EGGCategoryView"
                                                                      forIndexPath:indexPath];
    
    Tag *targetCatgegory = [self.categoryList objectAtIndex:indexPath.section];
    view.label.text = targetCatgegory.word;
    view.bounds = CGRectMake(0, 0, 200, 80);
    
    return view;
}

@end

//
//  Card.h
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 1..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Card : NSManagedObject

@property (nonatomic, retain) NSString * word;
@property (nonatomic, retain) NSString * imagepath;
@property (nonatomic, retain) NSString * thumbpath;

@end

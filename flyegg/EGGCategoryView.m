//
//  EGGCategoryView.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 2..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGCategoryView.h"

@implementation EGGCategoryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.label = [[UILabel alloc] initWithFrame:self.bounds];
        [self.label setTextAlignment:NSTextAlignmentLeft];
        [self addSubview:self.label];
    }
    return self;
}

- (void) prepareForReuse
{
    self.label.text = @"";
}

@end

//
//  EGGCategoryViewCell.h
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 2..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EGGTagViewCell : UICollectionViewCell

@property (nonatomic, strong) NSString *word;

@end

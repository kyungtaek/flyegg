//
//  Card.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 1..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "Card.h"


@implementation Card

@dynamic word;
@dynamic imagepath;
@dynamic thumbpath;

@end

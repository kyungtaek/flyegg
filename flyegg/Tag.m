//
//  Tag.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 1..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "Tag.h"
#import "Card.h"


@implementation Tag

@dynamic word;
@dynamic type;
@dynamic cards;

@end

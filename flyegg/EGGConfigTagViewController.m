//
//  EGGConfigCategoryViewController.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 2..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGConfigTagViewController.h"
#import "EGGTagViewCell.h"
#import "Tag.h"

@interface EGGConfigTagViewController ()

@end

@implementation EGGConfigTagViewController

- (id) init
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(200, 50)];
    [flowLayout setMinimumInteritemSpacing:2.f];
    [flowLayout setMinimumLineSpacing:0.f];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.sectionInset = UIEdgeInsetsMake(5.f, 0.f, 0.f, 0.f);
    
    self = [super initWithCollectionViewLayout:flowLayout];
    if (self) {
        
        self.tagList = [NSMutableArray array];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.collectionView registerClass:[EGGTagViewCell class]
            forCellWithReuseIdentifier:@"EGGCategoryViewCell"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UICollectionViewDatasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.tagList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    EGGTagViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EGGCategoryViewCell" forIndexPath:indexPath];
    
    Tag *targetTag = [self.tagList objectAtIndex:indexPath.row];
    [cell setWord:targetTag.word];
    
    return cell;
}

@end

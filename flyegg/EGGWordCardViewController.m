//
//  EGGWordCardViewController.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 1..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGWordCardViewController.h"
#import "EGGWordCardViewCell.h"
#import "EGGCardDataHelper.h"
#import "Card.h"

@interface EGGWordCardViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UICollectionView  *cardCollectionView;
@property (nonatomic, strong) UILabel           *pageLabelView;
@property (strong) NSArray                      *cardList;

@end

@implementation EGGWordCardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        EGGCardDataHelper *cardDataHelper = [[EGGCardDataHelper alloc] init];
        self.cardList = [cardDataHelper allCards];
        self.navigationItem.title = @"WordCard";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(600, 400);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.sectionInset = UIEdgeInsetsMake(184, 212, 184, 212);
    flowLayout.minimumLineSpacing = 424;
    
    self.cardCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768) collectionViewLayout:flowLayout];
    [self.cardCollectionView registerClass:[EGGWordCardViewCell class] forCellWithReuseIdentifier:@"EGGWordCardViewCell"];
    self.cardCollectionView.backgroundColor = [UIColor clearColor];
    self.cardCollectionView.pagingEnabled = YES;
    self.cardCollectionView.delegate = self;
    self.cardCollectionView.dataSource = self;
    self.cardCollectionView.scrollEnabled = YES;
    self.cardCollectionView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.cardCollectionView];
    
    self.pageLabelView = [[UILabel alloc] initWithFrame:CGRectMake(412, 700, 200, 50)];
    self.pageLabelView.backgroundColor = [UIColor blackColor];
    self.pageLabelView.textColor = [UIColor whiteColor];
    self.pageLabelView.font = [UIFont systemFontOfSize:18.0f];
    self.pageLabelView.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.pageLabelView];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    backButton.frame = CGRectMake(30, 30, 60, 60);
    [backButton setTitle:@"<" forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(actionBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) actionBackButton:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UICOllectionViewDatasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.cardList count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    EGGWordCardViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EGGWordCardViewCell" forIndexPath:indexPath];
    Card *targetCard = [self.cardList objectAtIndex:indexPath.row];
    [cell setWord:targetCard.word withWordImagePath:targetCard.imagepath];
    
    return cell;
}

@end

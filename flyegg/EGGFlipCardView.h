//
//  EGGFlipCardView.h
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 1..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EGGFlipCardView : UIView

@property (nonatomic, strong) NSString *wordImagePath;
@property (nonatomic, strong) NSString *word;

- (id) initWithFrame:(CGRect)frame;

- (void) flipToFrontWithAnimation:(BOOL)isAnimated;
- (void) flipToRearWithAnimation:(BOOL)isAnimated;

@end

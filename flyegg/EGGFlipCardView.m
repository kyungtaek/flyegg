//
//  EGGFlipCardView.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 1..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGFlipCardView.h"

@interface EGGFlipCardView()

@property (nonatomic, strong)   UIView *front;
@property (nonatomic, strong)   UIView *rear;
@property (nonatomic, weak)     UIView *currentFace;

@property (nonatomic, strong)   UIImageView *imageView;
@property (nonatomic, strong)   UILabel *label;

@end


@implementation EGGFlipCardView

- (id) initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if (self) {

        UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"card_back.png"]];
        bgView.frame = self.bounds;
        [self addSubview:bgView];
        
        self.layer.cornerRadius = 10.0f;
        self.layer.backgroundColor = [UIColor clearColor].CGColor;
        
        self.clipsToBounds = YES;
        
        [self addGestures];
        
        [self makeFrontView];
        [self makeRearView];
        
        self.word = @"";
        self.wordImagePath = @"";
    }
    
    return self;
}

- (void) addGestures
{
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(actionSwipe:)];
    [self addGestureRecognizer:gesture];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ationTap:)];
    [self addGestureRecognizer:tapGesture];
}

- (BOOL) isFront;
{
    return self.currentFace == self.front ? YES : NO;
}

- (BOOL) isRear;
{
    return self.currentFace == self.rear ? YES : NO;
}

- (void) actionSwipe:(id)sender;
{
    if ( [self isFront] == YES)
    {
        [self flipToRearWithAnimation:YES];
    }
    else
    {
        [self flipToFrontWithAnimation:YES];
    }
}

- (void) ationTap:(id)sender;
{
    if ( [self isFront] == YES)
    {
        [self flipToRearWithAnimation:YES];
    }
    else
    {
        [self flipToFrontWithAnimation:YES];
    }
}

- (void) makeFrontView;
{
    if ( self.front == nil ) {
        UIView *front = [[UIView alloc] initWithFrame:self.bounds];
        front.backgroundColor = [UIColor whiteColor];
        front.layer.cornerRadius = 25.0f;
        front.clipsToBounds = YES;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectInset(front.bounds, 30, 30)];
        
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.backgroundColor = [UIColor clearColor];
        self.imageView = imageView;
        
        [front addSubview:imageView];
        
        self.front = front;
    }
    
    // load image
}

- (void) makeRearView;
{
    if ( self.rear == nil ) {
        UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
        label.adjustsFontSizeToFitWidth = YES;
        label.numberOfLines = 1;
        label.font = [UIFont systemFontOfSize:84.f];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor blackColor];
        label.backgroundColor = [UIColor clearColor];
        self.label = label;
        
        self.rear = label;
    }
    
    UILabel *label = (UILabel *)self.rear;
    label.text = self.word;
}

- (void) setFront:(UIView *)front;
{
    [self.front removeFromSuperview];
    _front = nil;
    
    if ( front == nil) {
        return;
    }
    
    _front = front;
    
    [self addSubview:self.front];
    self.front.frame = [self calContentViewFrame];
}

- (void) setRear:(UIView *)rear;
{
    [self.rear removeFromSuperview];
    _rear = nil;
    
    if ( rear == nil) {
        return;
    }
    
    _rear = rear;
    
    [self addSubview:self.rear];
    self.rear.frame = [self calContentViewFrame];
}

- (void) setWord:(NSString *)word
{
    if (_word) {
        _word = nil;
    }
    
    if ( word ) {
        _word = [word copy];
        
        if ( self.label ) {
            self.label.text = word;
        }
    }
}

- (void) setWordImagePath:(NSString *)wordImagePath
{
    if (_wordImagePath) {
        _wordImagePath = nil;
    }
    
    if ( wordImagePath ) {
        _wordImagePath = [wordImagePath copy];

        if ( self.imageView ) {
            [self.imageView setImage:[UIImage imageNamed:@"card_image"]];
        }
    }
}

- (CGRect) calContentViewFrame;
{
    CGRect contentFrame = CGRectInset(self.bounds, 5, 5);
    return contentFrame;
}

- (void) flipToFrontWithAnimation:(BOOL)isAnimated
{
    if ( isAnimated ) {
        [UIView beginAnimations:@"CARD_FLIP" context:nil];
        
        [UIView setAnimationDuration:1.0f];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self cache:YES];
        self.rear.hidden = YES;
        self.front.hidden = NO;
        self.currentFace = self.front;
        
        [UIView commitAnimations];
    }
    else
    {
        self.rear.hidden = YES;
        self.front.hidden = NO;
        self.currentFace = self.front;
    }
}

- (void) flipToRearWithAnimation:(BOOL)isAnimated
{
    if ( isAnimated ) {
        [UIView beginAnimations:@"CARD_FLIP" context:nil];
        
        [UIView setAnimationDuration:1.0f];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self cache:YES];
        self.rear.hidden = NO;
        self.front.hidden = YES;
        self.currentFace = self.rear;
        
        [UIView commitAnimations];
    }
    else
    {
        self.rear.hidden = NO;
        self.front.hidden = YES;
        self.currentFace = self.rear;
    }
}


@end

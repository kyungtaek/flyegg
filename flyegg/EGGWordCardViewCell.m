//
//  EGGWordCardViewCell.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 1..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGWordCardViewCell.h"
#import "EGGFlipCardView.h"

@interface EGGWordCardViewCell()

@property EGGFlipCardView *flipCardView;

@end

@implementation EGGWordCardViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.autoresizesSubviews = YES;
        
        EGGFlipCardView *flipCardView = [[EGGFlipCardView alloc] initWithFrame:self.bounds];
        self.flipCardView = flipCardView;
        [self.flipCardView flipToRearWithAnimation:NO];
        
        [self addSubview:flipCardView];
        
    }
    return self;
}

- (void) setWord:(NSString *)word withWordImagePath:(NSString *)wordImagePath
{
    [self.flipCardView setWord:word];
    [self.flipCardView setWordImagePath:wordImagePath];
}

- (void) prepareForReuse
{
    [self.flipCardView flipToRearWithAnimation:NO];
}

@end

//
//  EGGThumbCardVIewCell.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 2..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGThumbCardVIewCell.h"

@interface EGGThumbCardVIewCell ()

@property UILabel *label;
@property UIImageView *imageview;
@end

@implementation EGGThumbCardVIewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
        [label setTextAlignment:NSTextAlignmentCenter];
        self.label = label;
        
        self.imageview = [[UIImageView alloc] initWithFrame:self.bounds];
        self.backgroundView = self.imageview;

    }
    return self;
}

- (void) prepareForReuse
{
    self.label.text = @"";
}

- (void) setWord:(NSString *)word
{
    self.label.text = word;
}

- (void) setThumbPath:(NSString *) thumbPath
{
    [self.imageview setImage:[UIImage imageNamed:@"card_image"]];
}

@end

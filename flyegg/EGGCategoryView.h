//
//  EGGCategoryView.h
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 2..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EGGCategoryView : UICollectionReusableView

@property UILabel *label;

@end

//
//  EGGLocalDataHelper.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 1..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGCardDataHelper.h"
#import "EGGCoreDataHelper.h"
#import "Card.h"
#import "Tag.h"

@interface EGGCardDataHelper()

@property EGGCoreDataHelper *coreDataHelper;

@end

@implementation EGGCardDataHelper

- (id) init
{
    self = [super init];
    if ( self ) {
        
        self.coreDataHelper = [EGGCoreDataHelper sharedInstance];
    }
    
    return self;
}

- (NSArray *) allChildren;
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *description = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:self.coreDataHelper.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@", TAGTYPE_CHILDREN];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"word" ascending:YES];
    
    [req setEntity:description];
    [req setPredicate:predicate];
    [req setSortDescriptors:@[sortDescriptor]];
    
    NSArray *results = nil;
    NSError *error = nil;
    results = [self.coreDataHelper.managedObjectContext executeFetchRequest:req error:&error];
    
    if ( error != nil || results == nil) {
        NSLog(@"Load error:\n%@", error);
        return [NSArray array];
    }
    
    return results;
}

- (NSArray *) allCategory;
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *description = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:self.coreDataHelper.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@", TAGTYPE_CATEGORY];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"word" ascending:YES];
    
    [req setEntity:description];
    [req setPredicate:predicate];
    [req setSortDescriptors:@[sortDescriptor]];
    
    NSArray *results = nil;
    NSError *error = nil;
    results = [self.coreDataHelper.managedObjectContext executeFetchRequest:req error:&error];
    
    if ( error != nil || results == nil) {
        NSLog(@"Load error:\n%@", error);
        return [NSArray array];
    }
    
    return results;
}
- (NSArray *) allTags;
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *description = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:self.coreDataHelper.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@", TAGTYPE_NORMAL];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"word" ascending:YES];
    
    [req setEntity:description];
    [req setPredicate:predicate];
    [req setSortDescriptors:@[sortDescriptor]];
    
    NSArray *results = nil;
    NSError *error = nil;
    results = [self.coreDataHelper.managedObjectContext executeFetchRequest:req error:&error];
    
    if ( error != nil || results == nil) {
        NSLog(@"Load error:\n%@", error);
        return [NSArray array];
    }
    
    return results;
}

- (NSArray *) allCards;
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *description = [NSEntityDescription entityForName:@"Card" inManagedObjectContext:self.coreDataHelper.managedObjectContext];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"word" ascending:YES];
    
    [req setEntity:description];
    [req setSortDescriptors:@[sortDescriptor]];
    
    NSArray *results = nil;
    NSError *error = nil;
    results = [self.coreDataHelper.managedObjectContext executeFetchRequest:req error:&error];
    
    if ( error != nil || results == nil) {
        NSLog(@"Load error:\n%@", error);
        return [NSArray array];
    }
    
    return results;
}


- (NSArray *) findCardsByChildren:(NSString *)childrenName isRandom:(BOOL)isRandom
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *description = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:self.coreDataHelper.managedObjectContext];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"word" ascending:YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@ and word = %@", TAGTYPE_CHILDREN, childrenName];

    [req setEntity:description];
    [req setPredicate:predicate];
    [req setSortDescriptors:@[sortDescriptor]];
    
    NSArray *results = nil;
    NSError *error = nil;
    results = [self.coreDataHelper.managedObjectContext executeFetchRequest:req error:&error];
    
    if ( error != nil || results == nil) {
        NSLog(@"Load error:\n%@", error);
        return [NSArray array];
    }
    
    if ( results.count == 0) {
        NSLog(@"No exist");
        return [NSArray array];
    }
    
    Tag *foundChildren = [results objectAtIndex:0];
    NSArray *cards = foundChildren.cards.array;
    
    return cards;
}

- (NSArray *) findCardsByCategory:(NSString *)categoryName isRandom:(BOOL)isRandom;
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *description = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:self.coreDataHelper.managedObjectContext];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"word" ascending:YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@ and word = %@", TAGTYPE_CATEGORY, categoryName];
    
    [req setEntity:description];
    [req setPredicate:predicate];
    [req setSortDescriptors:@[sortDescriptor]];
    
    NSArray *results = nil;
    NSError *error = nil;
    results = [self.coreDataHelper.managedObjectContext executeFetchRequest:req error:&error];
    
    if ( error != nil || results == nil) {
        NSLog(@"Load error:\n%@", error);
        return [NSArray array];
    }
    
    if ( results.count == 0) {
        NSLog(@"No exist");
        return [NSArray array];
    }
    
    Tag *foundCategory = [results objectAtIndex:0];
    NSArray *cards = foundCategory.cards.array;
    
    return cards;
    
}
- (NSArray *) findCardsByTag:(NSString *)tagName isRandom:(BOOL)isRandom;
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *description = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:self.coreDataHelper.managedObjectContext];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"word" ascending:YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@ and word = %@", TAGTYPE_NORMAL, tagName];
    
    [req setEntity:description];
    [req setPredicate:predicate];
    [req setSortDescriptors:@[sortDescriptor]];
    
    NSArray *results = nil;
    NSError *error = nil;
    results = [self.coreDataHelper.managedObjectContext executeFetchRequest:req error:&error];
    
    if ( error != nil || results == nil) {
        NSLog(@"Load error:\n%@", error);
        return [NSArray array];
    }
    
    if ( results.count == 0) {
        NSLog(@"No exist");
        return [NSArray array];
    }
    
    Tag *foundTag = [results objectAtIndex:0];
    NSArray *cards = foundTag.cards.array;
    
    return cards;
}

- (NSArray *) findCardsByWord:(NSString *)word;
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *description = [NSEntityDescription entityForName:@"Card" inManagedObjectContext:self.coreDataHelper.managedObjectContext];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"word" ascending:YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"word = %@", word];
    
    [req setEntity:description];
    [req setPredicate:predicate];
    [req setSortDescriptors:@[sortDescriptor]];
    
    NSArray *results = nil;
    NSError *error = nil;
    results = [self.coreDataHelper.managedObjectContext executeFetchRequest:req error:&error];
    
    if ( error != nil || results == nil) {
        NSLog(@"Load error:\n%@", error);
        return [NSArray array];
    }
    
    if ( results.count == 0) {
        NSLog(@"No exist");
        return [NSArray array];
    }
    
    return results;
}

- (Card *) addNewCard:(NSString *)word
        withImagePath:(NSString *)imagePath
        withThumbPath:(NSString *)thumbPath;
{
    Card *card = [NSEntityDescription insertNewObjectForEntityForName:@"Card" inManagedObjectContext:self.coreDataHelper.managedObjectContext];
    card.word = word;
    card.imagepath = imagePath;
    card.thumbpath = thumbPath;
    
    [self.coreDataHelper saveContext];
    
    return card;
}

- (Card *) updateCard:(Card *)targetCard
          withNewWord:(NSString *)newWord
        withImagePath:(NSString *)newImagePath
        withThumbPath:(NSString *)newThumbPath;
{
    targetCard.word = newWord;
    targetCard.imagepath = newImagePath;
    targetCard.thumbpath = newThumbPath;
    
    [self.coreDataHelper saveContext];
    
    return targetCard;
}

- (void) deleteCard:(Card *)targetCard;
{
    [self.coreDataHelper.managedObjectContext delete:targetCard];
    [self.coreDataHelper saveContext];
}

- (Card *) registerTag:(Tag *)tag withCard:(Card *)targetCard;
{
    [tag addCardsObject:targetCard];
    [self.coreDataHelper saveContext];
    
    return targetCard;
}

- (Card *) removeTag:(Tag *)tag withCard:(Card *)targetCard;
{
    for ( Card *card in tag.cards ) {
        
        if ( targetCard.objectID == card.objectID ) {
            [tag removeCardsObject:card];
            break;
        }
    }
    
    return targetCard;
}

- (Tag *) addNewTag:(NSString *)word
        withTagType:(NSString *)tagType;
{
    Tag *tag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext:self.coreDataHelper.managedObjectContext];
    tag.word = word;
    tag.type = tagType;
    
    [self.coreDataHelper saveContext];
    
    return tag;
}

- (Tag *) updateTag:(Tag *)targetTag
        withNewWord:(NSString *)word
        withTagType:(NSString *)tagType;
{
    targetTag.word = word;
    targetTag.type = tagType;
    
    [self.coreDataHelper saveContext];
    
    return targetTag;
}

- (void) deleteTag:(Tag *)targetTag;
{
    [self.coreDataHelper.managedObjectContext delete:targetTag];
    
    [self.coreDataHelper saveContext];
}
@end

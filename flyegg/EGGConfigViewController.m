//
//  EGGConfigViewController.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 1..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGConfigViewController.h"
#import "EGGCardDataHelper.h"
#import "EGGConfigCardCollectionViewController.h"
#import "EGGConfigTagViewController.h"
#import "Card.h"
#import "Tag.h"

@interface EGGConfigViewController ()

@property UICollectionView *cardCollectionView;
@property (strong) NSMutableArray *categoryList;
@property (strong) NSMutableArray *tagList;
@property EGGCardDataHelper *cardHelper;

@property EGGConfigCardCollectionViewController *cardCollectionViewController;
@property EGGConfigTagViewController *tagViewController;

@end

@implementation EGGConfigViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.navigationItem.title = @"Configuration";
        self.cardHelper = [[EGGCardDataHelper alloc] init];
        self.categoryList = [NSMutableArray arrayWithArray:[self.cardHelper allCategory]];
        self.tagList = [NSMutableArray arrayWithArray:[self.cardHelper allTags]];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tagViewController = [[EGGConfigTagViewController alloc] init];
    [self addChildViewController:self.tagViewController];
    [self.view addSubview:self.tagViewController.view];
    self.tagViewController.tagList = self.tagList;

    self.cardCollectionViewController = [[EGGConfigCardCollectionViewController alloc] init];
    [self addChildViewController:self.cardCollectionViewController];
    [self.view addSubview:self.cardCollectionViewController.view];
    self.cardCollectionViewController.categoryList = self.categoryList;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.tagViewController.view.frame = CGRectMake(0, 0, 1024, 150);
    self.cardCollectionViewController.view.frame = CGRectMake(0, 150, 1024, 618);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

//
//  EGGCategoryViewCell.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 2..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGTagViewCell.h"

@interface EGGTagViewCell()

@property UILabel *label;

@end

@implementation EGGTagViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.label = [[UILabel alloc] initWithFrame:self.bounds];
        [self.label setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:self.label];
    }
    return self;
}

- (void) setWord:(NSString *)word
{
    if ( _word ) {
        _word = nil;
    }
    
    if ( word ) {
        _word = [word copy];
    }
    
    self.label.text = _word;
}

@end

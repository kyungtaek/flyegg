//
//  EGGIntroViewController.m
//  flyegg
//
//  Created by kyungtaek on 2013. 11. 2..
//  Copyright (c) 2013년 eggteam. All rights reserved.
//

#import "EGGIntroViewController.h"
#import "EGGWordCardViewController.h"
#import "EGGConfigViewController.h"
#import "EGGCardDataHelper.h"
#import "Tag.h"
#import "Card.h"

@interface EGGIntroViewController ()

@end

@implementation EGGIntroViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        EGGCardDataHelper *cardDataHelper = [[EGGCardDataHelper alloc] init];
//
//        [cardDataHelper addNewCard:@"참새"
//                     withImagePath:@""
//                     withThumbPath:@""];
//        
//        [cardDataHelper addNewCard:@"뱁새"
//                     withImagePath:@""
//                     withThumbPath:@""];
//
//        [cardDataHelper addNewCard:@"황새"
//                     withImagePath:@""
//                     withThumbPath:@""];
//
//        [cardDataHelper addNewCard:@"흰눈썹울새"
//                     withImagePath:@""
//                     withThumbPath:@""];
//
//        [cardDataHelper addNewCard:@"솔새"
//                     withImagePath:@""
//                     withThumbPath:@""];
//
//        [cardDataHelper addNewCard:@"유리딱새"
//                     withImagePath:@""
//                     withThumbPath:@""];
//
//        [cardDataHelper addNewCard:@"노랑턱멧새"
//                     withImagePath:@""
//                     withThumbPath:@""];
//
//        [cardDataHelper addNewCard:@"진박새"
//                     withImagePath:@""
//                     withThumbPath:@""];
//
//        [cardDataHelper addNewCard:@"박새"
//                     withImagePath:@""
//                     withThumbPath:@""];
//
//        [cardDataHelper addNewCard:@"흰목물떼새"
//                     withImagePath:@""
//                     withThumbPath:@""];
//        
//        [cardDataHelper addNewTag:@"새 이름" withTagType:TAGTYPE_CATEGORY];
        
        
//        NSArray *cards = [cardDataHelper allCards];
//    
//        Tag *newCategory = [cardDataHelper addNewTag:@"개 카테고리" withTagType:TAGTYPE_CATEGORY];
//        for ( Card * card in cards) {
//            [cardDataHelper registerTag:newCategory withCard:card];
//        }
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *wordCardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [wordCardButton setFrame:CGRectMake(300, 300, 200, 200)];
    [wordCardButton setBackgroundColor:[UIColor lightGrayColor]];
    [wordCardButton setTitle:@"WordCard" forState:UIControlStateNormal];
    [wordCardButton addTarget:self action:@selector(startWordCardGame) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:wordCardButton];

    UIButton *cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cameraButton setFrame:CGRectMake(600, 300, 200, 200)];
    [cameraButton setBackgroundColor:[UIColor lightGrayColor]];
    [cameraButton setTitle:@"Camera" forState:UIControlStateNormal];
    [self.view addSubview:cameraButton];

    UIButton *pairGameButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [pairGameButton setFrame:CGRectMake(300, 500, 200, 200)];
    [pairGameButton setBackgroundColor:[UIColor lightGrayColor]];
    [pairGameButton setTitle:@"PairGame" forState:UIControlStateNormal];
    [self.view addSubview:pairGameButton];

    UIButton *configButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [configButton setFrame:CGRectMake(600, 500, 200, 200)];
    [configButton setBackgroundColor:[UIColor lightGrayColor]];
    [configButton setTitle:@"Config" forState:UIControlStateNormal];
    [configButton addTarget:self action:@selector(startConfiguration) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:configButton];

}

- (void) startWordCardGame
{
    EGGWordCardViewController *wordCardViewController = [[EGGWordCardViewController alloc] init];
    [self.navigationController pushViewController:wordCardViewController animated:YES];
}

- (void) startConfiguration
{
    EGGConfigViewController *configViewController = [[EGGConfigViewController alloc] init];
    [self.navigationController pushViewController:configViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
